Source: ltp
Section: misc
Priority: optional
Maintainer: Apertis Packagers <devel@lists.apertis.org>
Build-Depends:
 debhelper-compat (= 12),
 libclone-perl,
 libcommon-sense-perl,
 libencode-locale-perl,
 libhttp-date-perl,
 libhttp-message-perl,
 libjson-perl,
 libjson-xs-perl,
 libtirpc-dev,
 libtry-tiny-perl,
 libtypes-serialiser-perl,
 liburi-perl,
 libwww-perl,
 mawk,
 pkg-config,
 tzdata,
Standards-Version: 4.5.1
Homepage: http://linux-test-project.github.io/
Vcs-Browser: https://gitlab.apertis.org/pkg/ltp
Vcs-Git: https://gitlab.apertis.org/pkg/ltp.git

Package: ltp-commands-test
Architecture: any
Depends:
 ltp-tools,
 python3,
 expect,
 ksh | mksh | pdksh | zsh,
 tcsh | csh | c-shell,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Command tests for the Linux Test Project
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains files for testing general commands in Debian
 using the ltp or debian-test programs.

Package: ltp-cve-test
Architecture: any
Depends:
 ltp-tools,
 ${misc:Depends},
 ${shlibs:Depends},
Description: CVE tests for the Linux Test Project
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains the CVE tests for the Linux Test Project.

Package: ltp-disk-test
Architecture: any
Depends:
 ltp-tools,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Disk I/O tests for the Linux Test Project
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains the disk i/o tests for the Linux Test Project.

Package: ltp-kernel-test
Architecture: any
Depends:
 ltp-tools,
 python3,
 ${misc:Depends},
 ${shlibs:Depends},
Description: kernel tests for the Linux Test Project
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains tests for testing the Linux Kernel using the
 ltp or debian-test packages.

Package: ltp-misc-test
Architecture: any
Depends:
 ltp-tools,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Misc. tests for the Linux Test Project
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains files for testing miscellaneous commands in
 Debian using the ltp or debian-test programs.

Package: ltp-network-test
Architecture: any
Depends:
 ltp-tools,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Network tests for the Linux Test Project
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains files for testing the Networking commands in
 Debian using the ltp or debian-test programs.

Package: ltp-tools
Architecture: any
Depends:
 python3,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Utilities for running the Linux Test Project test suite
 The Linux Test Project is a joint project started by SGI, developed
 and maintained by IBM, Cisco, Fujitsu, SUSE, Red Hat and others,
 that has a goal to deliver test suites to the open source community
 that validate the reliability, robustness, and stability of Linux.
 The LTP testsuite contains a collection of tools for testing the
 Linux kernel and related features.
 .
 This package contains utilities used by the test suite.
